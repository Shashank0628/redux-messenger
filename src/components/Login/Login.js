import { Button } from "@material-ui/core";
import React from "react";
import style from "./Login.module.css";
import { auth, provider } from "../../firebaseConfig";

const Login = () => {
  const signIn = () => {
    auth.signInWithPopup(provider).catch((err) => {
      alert(err.message);
    });
  };
  return (
    <div className={style.login}>
      <img
        src="data:image/webp;base64,UklGRiwIAABXRUJQVlA4ICAIAADwQwCdASrhAOEAPtForFAoJqQipxRJMQAaCWNu/EbbZlnrOG9nyI/5/eofZ8l/Zfxu6Xfqnpa6ltFHsB8pZ6P8f6ivMF8/HoX8wHnS+mf+59P/603oAdMf/c8kJ8nLBnSa3X2qzLAcfK/+5WT88rJ84Vea73He+zPlVfPd3xQw9biv8J6XBPBsg2w65waNP767ZiAd+HS6wk0wmQT/c1+9/0w68DsGBDCMB0y6PyLZuRQdyS+UYnH1AVVJk8fHx8dLNNU8KUFiMY6ZYIK57pENuyUS6AdaBWZ2KKBx/2f6wLgH1tL76JZRFgGd7fX+DhYapEOFe/Ulm2/4JpdFh7CPZcXEY3Y/qNzeKWLsymRbUgyJAzzR/Dqbg3Nr1p+p8mTf1clprQ7XS4PUOhS+1sNcJJI7FkFjBne/LFPiEXGdAjoDYy4r548jYu8vJ1Tqs3fpKfwWRaRRnhx5S0XcyqiHrRuVaiXkqvfNA16PbQBmy+gIGcUZHbckQeaO501cDQvGkbiHKw3oda/P3iP5vEL6ZThont+gH1fRL9+OJyx6EWkfUFCqYZ80sQJDersjYjDPhvP5NbB/EyFj1Mznd3ZqGz1Nmcz2NKjvcewsuy25mSAxbnr9omYCYrlSO3oHhqd/71p0L/vw7O+j9kwUzpGb1lfVQGwtRAm5KqPSbOICmGcBx+IyYVcCuKShNTCYdHGdJJgpwYlx6umLH8+IrBTqbOdCusJceMAA/qtxX+vUH/y0uf9eoP/lpc/69Qf/LS5/16g/+Wlz/r1B/8tLn/XqD/5aXP+vUH/y0cCLSO/KQiNz9f94+zuvp/RU9eXDmqo2HaYld5z+4Ksj/5AdlFVV4VFkksvCO/DaW++TlH1oIstrzKVKjrdUOcf/9wKTjgsxjM77e5rj1I+THw8TNCsPsslhpKnQ4sEZoN5yNCtvi6yt6HCN+7YjL4KdPffNb43cjNUUHizAGdAxUNbL8Xn9rGFD+ps+e0ZkbM2H5BuMVH09nobvGL64jS1lK/bH7H2qwtOO/NE6yjUzxXjG4KwSHNF9HP1HAs7TTTO6LqSOhdrjR53fw/7rMp3VJbz1siWWiw/dknODO+sYZL/fOnepc/L+cgPPMKmd6T+GO0rQlB8qgUdEGORFFw0tqg8nQDtCz87neKC1PkVd/HdBuIDJpHlvdcoB1lXg0Q2DRA0QNEDRA0QNEDRA6bSufHi0HAKYb+A8y6/8zqiC+6EoQB50TjvulTBq9beV0WJq8DQv6aF+iYxZ50pR5Dozo5vbT4Hd3Md/wGlz4NPfVTXmfuD7jvyPZ29uFUR5/82v181jFZTTD/2n09E8GBrZ3bgRZDpIoCZFeUHTbom1YItB6DFdLfkQzRK7jB8aAJxZ0XQQEfMVSmCD1PYh7VFiv7Q0WctEH0xDbaGIg+kGf4pMeSU1muVryYvxJJLZv6Ybh5CESfWE9oSNers0isHmjZ2C+8oyBDZn/Wng715gl62sdxWbYqu5i3fb5hF1yP3tWl3y97BqzDhu+FihTh8R3m2i5rmW8jUDiMnAyG2u2+0gsSDDygMi6OfSgEG7mhlylTthUaeOUHaCOVaM5NQaAA9MHjfJPtdLg2IIHm5LoyBZneZZl8Bw1xf4afeAdQwXEoG0kpFGlr7U5MfJwBnpfrYpYJLKxAVDNLjGwXnjeCQxKaVjjEbR/4bxmUOqLydTfLLurcYW4p0TXP/cGOE/Zntl3Y5+QUzWUEvl2kp8Qccd0D4EoJbpW/MBZU0QGZOyKPzKat0nOiXzNVCs+QOH4nTzRlbKEwM3uY/6wigFZozB75ElCbGivZx4nNSoKCJYdndeZ69AqnYjlnlKtcbXwJxxtzcy8PQtFkXFdBQU6iEfsfIAoaLu+5/1S4FSSlb8u5qFou6EMOYo2DpU//vv0B2XHHwB4SlErBfIP6eW+Y43qvf8UX4uJOQOfEbDA34dRsFXeAC4aZTht2dbEkBuj3tJaV2n1NYEq6RCPN288Om1TR09jdmswshdlGbf9JJPzBtnLEN0XMTTbWfey+9+20LcPoaF6vdeqx9TJS01SdovTeUgET/4elEGqDsYIXqnY/IkKRoriYlVHaRe2ozsxF4l2B+w17DrUwRLb5QEG7uBFOesjpDLtDE717FqiAw/ulEHuDbg3rXcCWoSLmdfVBjUXNg0WJkAkSKbna/lkXW6BqvqrDvTTElqkNsGk6fPzoUK2vuZHcPTnsgYuW294KwW53AZDc2q2NBCHOJBUKbUuMGhhPfpj4NappyRFJtBKk2/Vy5Dun2KBDyRfWpfAYUb6wIEiCywbaFwc0lOwAKEq50g71Fg6jJ+BFVAIVVdaYx11F5n9Yv6wZi9ofo/TduoRpd4mGb2ffrCjA7zOjz6yjF7Y9Se9UPMTj4HeRfsMB4HgJdrvCONy9RDxhxEYK1OK7SPjKmhQtwQGyWU2B5TY0As+hNHjhFtyuI3+Fj7wZ/BrOCbrrNt5QwpO3/xrAq6Z9VAyhh7vzXMEh3xJnSWwEI46jQ+rbt6yhgWcT+uR0hta/szmBbmb+6nGBLm54bOKAtm1laWoGoHomo3Q0GAsWN1eRlGR9o3Ojl5Qas0hqOvfZBejrCA+zcrGujspfzvaDM9oxhaQLVXvs2GrXF9QYpx6VjyUzxc68fc6palLy1RraGFZ3bmpiBXOc+ekNisnngl6mfTJE54vy8tVooxtQZ6km2K9ipUI9No2oAvxF+AevsCQUeQag8yafxef+eyxH8EY2ejrlWVfYjK9PjvDfAA"
        alt=""
      />
      <h1>iMessage Clone</h1>
      <Button variant="contained" color="primary" onClick={signIn}>
        Sign In
      </Button>
    </div>
  );
};

export default Login;
