import React, { useEffect } from "react";
import "./App.css";
import SideBar from "./iMessage/SideBar/SideBar";
import ChatBar from "./iMessage/ChatBar/ChatBar";
import { useSelector, useDispatch } from "react-redux";
import { login, logout } from "../redux/actions";
import Login from "./Login/Login";
import { auth } from "../firebaseConfig";

const App = () => {
  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();
  useEffect(() => {
    auth.onAuthStateChanged((authUser) => {
      if (authUser) {
        dispatch(
          login({
            uid: authUser.uid,
            photo: authUser.photoURL,
            displayName: authUser.displayName,
            email: authUser.email,
          })
        );
      } else {
        dispatch(logout());
      }
    });
  }, []);
  return (
    <div className="App">
      {user ? (
        <>
          <SideBar />
          <ChatBar />
        </>
      ) : (
        <Login />
      )}
    </div>
  );
};

export default App;
