import React, { useState, useEffect } from "react";
import style from "./ChatBar.module.css";
import MicNoneIcon from "@material-ui/icons/MicNone";
import { IconButton } from "@material-ui/core";
import Message from "./Message/Message";
import { useSelector } from "react-redux";
import db from "../../../firebaseConfig";
import firebase from "firebase";
import FlipMove from "react-flip-move";

const ChatBar = () => {
  const [input, setInput] = useState("");
  const chatName = useSelector((state) => state.chat.chatName);
  const chatID = useSelector((state) => state.chat.chatID);
  const [messages, setMessages] = useState([]);
  const user = useSelector((state) => state.user);

  useEffect(() => {
    if (chatID) {
      db.collection("chats")
        .doc(chatID)
        .collection("messages")
        .orderBy("timestamp", "desc")
        .onSnapshot((snapshot) => {
          const finalMessages = snapshot.docs.map((message) => ({
            id: message.id,
            data: message.data(),
          }));
          setMessages(finalMessages);
        });
    }
  }, [chatID]);

  const sendMessage = (e) => {
    e.preventDefault();
    if (chatID) {
      db.collection("chats").doc(chatID).collection("messages").add({
        message: input,
        timestamp: firebase.firestore.FieldValue.serverTimestamp(),
        uid: user.uid,
        displayName: user.displayName,
        photo: user.photo,
        email: user.email,
      });
    }
    setInput("");
  };
  return (
    <div className={style.chatBar}>
      <div className={style.header}>
        <h3>
          To: <span className={style.cname}>{chatName ? chatName : " "}</span>
        </h3>
        <strong>Details</strong>
      </div>

      <div className={style.messages}>
        <FlipMove>
          {messages.map((message) => (
            <Message key={message.id} content={message.data} />
          ))}
        </FlipMove>
      </div>
      <div className={style.input}>
        <form action="" onSubmit={sendMessage}>
          <input
            type="text"
            placeholder="Imessage"
            value={input}
            onChange={(e) => setInput(e.target.value)}
          />
          <button>Send</button>
          <IconButton>
            <MicNoneIcon />
          </IconButton>
        </form>
      </div>
    </div>
  );
};

export default ChatBar;
