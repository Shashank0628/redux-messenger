import React, { forwardRef } from "react";
import { Avatar } from "@material-ui/core";
import style from "./Message.module.css";
import { useSelector } from "react-redux";
import * as timeago from "timeago.js";

const Message = forwardRef(({ id, content }, ref) => {
  const user = useSelector((state) => state.user);
  return (
    <div
      className={
        style.messageBox +
        " " +
        (user.email === content.email ? style.sender : "")
      }
      ref={ref}
    >
      <Avatar src={content.photo} className={style.photo} />
      <p>{content.message}</p>
      <small>{timeago.format(new Date(content.timestamp?.toDate()))}</small>
    </div>
  );
});

export default Message;
