import React, { useState, useEffect } from "react";
import { Avatar, IconButton } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import RRI from "@material-ui/icons/RateReviewOutlined";
import SideBarChat from "./SideBarChat/SideBarChat";
import style from "./SideBar.module.css";
import db, { auth } from "../../../firebaseConfig";
import { useSelector } from "react-redux";

const SideBar = () => {
  const user = useSelector((state) => state.user);
  const [chats, setChats] = useState([]);
  const takeChat = () => {
    const input = prompt("Enter the Chat Name");
    if (input) {
      db.collection("chats").add({ chatName: input });
    }
  };

  useEffect(() => {
    db.collection("chats").onSnapshot((snapshot) => {
      console.log("In Snapshot\n", snapshot);
      const finalChats = snapshot.docs.map((chat) => {
        return { id: chat.id, data: chat.data() };
      });
      setChats(finalChats);
    });
  }, []);

  return (
    <div className={style.sidebar}>
      <div className={style.header}>
        <Avatar
          src={user.photo}
          className={style.avatar}
          onClick={() => auth.signOut()}
        ></Avatar>
        <div className={style.input}>
          <SearchIcon />
          <input type="text" placeholder="search" />
        </div>
        <IconButton onClick={takeChat}>
          <RRI />
        </IconButton>
      </div>
      <div className={style.chats}>
        {chats.map((chat) => (
          <SideBarChat
            key={chat.id}
            chatName={chat.data.chatName}
            id={chat.id}
          />
        ))}
      </div>
    </div>
  );
};

export default SideBar;
