import React, { useState, useEffect } from "react";
import { Avatar } from "@material-ui/core";
import style from "./SideBarChat.module.css";
import { useDispatch } from "react-redux";
import { setChat } from "../../../../redux/actions";
import db from "../../../../firebaseConfig";
import * as timeago from "timeago.js";

const SideBarChat = ({ id, chatName }) => {
  const dispatch = useDispatch();
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    db.collection("chats")
      .doc(id)
      .collection("messages")
      .orderBy("timestamp", "desc")
      .onSnapshot((snapshot) => {
        const finalMessages = snapshot.docs.map((doc) => doc.data());
        setMessages(finalMessages);
      });
  }, []);

  const clickHandler = () => {
    dispatch(setChat({ chatID: id, chatName: chatName }));
  };
  return (
    <div className={style.sideBarChat} onClick={clickHandler}>
      <Avatar src={messages[0]?.photo} className={style.avatar} />
      <div className={style.infoBox}>
        <div className={style.info}>
          <h3>{chatName}</h3>
          <p>{messages[0]?.message}</p>
        </div>
        <small>
          {timeago.format(new Date(messages[0]?.timestamp?.toDate()))}
        </small>
      </div>
    </div>
  );
};

export default SideBarChat;
