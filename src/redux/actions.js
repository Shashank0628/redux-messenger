export const login = (user) => {
  return {
    type: "LOGIN",
    payload: user,
  };
};

export const logout = () => {
  return {
    type: "LOGOUT",
  };
};

export const setChat = (chat) => {
  return {
    type: "SET_CHAT",
    payload: chat
  }
}