export const InitialState = {
  user: null,
  chat: {
    chatID: null,
    chatName: null,
  },
};
