import { InitialState } from "../initialState";
export const userReducer = (state = InitialState.user, action) => {
  switch (action.type) {
    case "LOGIN":
      return action.payload;
    case "LOGOUT":
      return null;
    default:
      return state;
  }
};
