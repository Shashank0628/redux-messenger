import { InitialState } from "../initialState";

export const chatReducer = (state = InitialState.chat, action) => {
  switch (action.type) {
    case "SET_CHAT":
      return {
        chatID: action.payload.chatID,
        chatName: action.payload.chatName,
      };

    default: {
      return state;
    }
  }
};
